class ChatMessage < ApplicationRecord
  belongs_to :chapter
  belongs_to :user

  def as_json(*)
    super.merge(user_login: user.login, user_color: user.color)
  end
end
