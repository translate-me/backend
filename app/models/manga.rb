class Manga < ApplicationRecord
  has_many :chapters, dependent: :destroy

  has_one_attached :cover

  def translated_links
    self[:translated_links] || []
  end

  def original_links
    self[:original_links] || []
  end
end
