class Page < ApplicationRecord
  include Rails.application.routes.url_helpers

  before_save :set_index, if: proc { |t| t.index.nil? }

  has_one_attached :original_image
  has_one_attached :translated_image
  belongs_to :chapter
  has_many :page_texts, dependent: :destroy

  default_scope { order(index: :asc) }

  def as_json(a)
    { original_image: original_image.attached??rails_blob_path(original_image, disposition: "attachment", only_path: true) : "",
      translated_image: translated_image.attached?? rails_blob_path(translated_image, disposition: "attachment", only_path: true) : "",
      index: index}
  end

  def translated_percent
    page_texts.translated.count.to_f / (page_texts.count.to_f / 100)
  end

  def set_index
    max = 0
    chapter.pages.each do |page|
      if page.index && page.index > max
        max = page.index
      end
    end
    self.index = max + 1
  end
end
