class Chapter < ApplicationRecord
  has_many :pages, dependent: :destroy
  has_many :chat_messages, dependent: :destroy
  has_many :page_texts, through: :pages, dependent: :destroy
  belongs_to :manga

  default_scope { order(number: :asc) }

  def translated_percent
    return 0 if page_texts.all.empty?

    page_texts.translated.count.to_f / (page_texts.count.to_f / 100)
  end

  def correct_number
    number == number.to_i ? number.to_i : number
  end
end
