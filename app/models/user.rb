require 'digest'
require 'securerandom'

class User < ApplicationRecord
  before_save :hash_password, if: proc { |u| u.password_changed? }

  validates :login, uniqueness: true

  def as_json(*)
    super.except('password', 'password_salt', 'updated_at')
  end

  def hash(s)
    Digest::SHA256.hexdigest password_salt + s
  end

  def check_password(s)
    return nil unless s.instance_of? String

    hash(s) == password
  end

  private

  def hash_password
    self.password_salt = SecureRandom.hex
    self.password = hash(password)
  end
end
