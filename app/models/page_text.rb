class PageText < ApplicationRecord
  belongs_to :page

  before_save :set_index, if: proc { |t| t.index.nil? }
  # before_destroy :update_indexes_on_destroy, if: proc { |t| !t.index.nil? }
  before_create :update_indexes_on_set

  default_scope { order(index: :asc) }

  scope :translated, -> { where(is_translated: true) }
  scope :not_translated, -> { where(is_translated: false) }

  private

  def set_index
    max = 0
    page.page_texts.each do |text|
      if text.index && text.index > max
        max = text.index
      end
    end
    self.index = max + 1
  end

  def update_indexes_on_destroy
    page.page_texts.each do |text|
      if text.index && text.index > self.index
        text.update(index: text.index - 1)
      end
    end
  end

  def update_indexes_on_set
    next_text = page.page_texts.where("index >= ?", [self.index]).first
    return unless next_text
    next_text.update(next_text.index + 1)
  end
end
