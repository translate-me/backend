class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :check_user_current
  before_action :process_id

  def api_render(args = {})
    defaults = {
        status: true,
        code: 200,
        errors: {},
        result: {}
    }
    args = defaults.merge(args)
    if args[:status].nil?
      if args.code >= 200 && args.code < 300
        args[:status] = true
      else
        args[:status] = false
      end
    end
    render json: {
        status: args[:status] ? 'ok' : 'fail',
        code: args[:code],
        errors: args[:errors],
        result: args[:result]
    }, status: args[:code]
  end


  def check_user_current
    unless session[:user_id]
      @user_current = nil
      return
    end
    @user_current = nil

    @user_current = User.find(session[:user_id])
  end

  def logged_in?
    !@user_current.nil?
  end

  def require_login
    unless logged_in?
      return api_render errors: {user: ['forbidden']}, status: false, code: 403
    end
  end

  def process_id
    if params[:chapter_number] # && !params[:chapter_id].match('\A(\d+)\.(\d+)\Z').nil?
      params[:chapter_id] = params[:chapter_number].to_f
    end
  end

end
