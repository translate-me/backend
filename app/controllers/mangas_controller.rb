class MangasController < ApplicationController
  def index
    @mangas = Manga.all
  end

  def show
    @manga = Manga.find(params[:id])
  end

  def new
  end

  def create
    @manga = Manga.new(manga_params)
    return api_render code: 403, errors: @manga.errors unless @manga.save

    api_render code: 201, result: { manga: @manga }
  end

  def edit
  end

  def update
    @manga = Manga.find(params[:id])
    return api_render code: 404 unless @manga

    if manga_params[:cover]
      @manga.cover.attach(manga_params[:cover])
    end

    return api_render code: 403, errors: @manga.errors unless @manga.update(manga_params)

    api_render
  end

  def destroy
    @manga = Manga.find(params[:id])
    return api_render code: 404 unless @manga

    @manga.destroy
    api_render
  end

  private

  def manga_params
    params.require(:manga).permit!
  end
end
