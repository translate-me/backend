class ChatMessagesController < ApplicationController
  def create
    @chapter = Chapter.find(params[:chapter_id])
    return api_render code: 404 unless @chapter

    @message = ChatMessage.create!(text: message_params[:text], chapter: @chapter, user: @user_current)
    REDIS_CLIENT.publish('chat/chapter', JSON(@message.as_json))
    api_render result: { message: @message }
  end

  private

  def message_params
    params.require(:message).permit([:text])
  end
end
