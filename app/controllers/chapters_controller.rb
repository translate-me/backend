class ChaptersController < ApplicationController


  def show
    @manga = Manga.find(params[:manga_id])
    return api_render code: 404 unless @manga
    @chapter = @manga.chapters.find_by_number(params[:number])
    @colors = {}
    User.all.each do |u|
      @colors[u.id] = '#' + u.color
    end
  end

  def create
    @manga = Manga.find(params[:manga_id])
    return api_render code: 404 unless @manga

    @chapter = @manga.chapters.new(chapter_params)
    return api_render code: 403, errors: @chapter.errors unless @chapter.save

    @page = @chapter.pages.create(index: 1)
    @text = @page.page_texts.create

    api_render code: 201
  end

  def update
    @chapter = Chapter.find(params[:id])
    return api_render code: 404 unless @chapter

    return api_render code: 403, errors: @chapter.errors unless @chapter.update(chapter_params)
    api_render
  end

  def destroy
    @chapter = Chapter.find(params[:id])
    return api_render code: 404 unless @chapter

    @chapter.destroy
    api_render
  end

  private

  def chapter_params
    params.require(:chapter).permit([:number, :title])
  end
end
