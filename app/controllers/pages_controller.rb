class PagesController < ApplicationController
  def show
    @manga = Manga.find(params[:manga_id])
    return api_render code: 401 unless @manga


    @chapter = @manga.chapters.find_by_number(params[:chapter_number].to_f)
    puts @chapter.nil?, params
    return api_render code: 402 unless @chapter

    @page = @chapter.pages.find_by_index(params[:page_index])
    return api_render code: 403 unless @page
  end

  def create
    @chapter = Chapter.find(params[:chapter_id])
    return api_render code: 404 unless @chapter

    if !page_params[:index].nil? && @chapter.pages.length>0 && page_params[:index].to_i >= @chapter.pages.last.index
      @chapter.pages.where("index >= ?", page_params[:index].to_i).each do |page|
        page.index += 1
        page.save
      end
    end


    # puts page_params
    # puts index: page_params[:index].nil? ? 1 : page_params[:index]
    # index = 1
    # if page_params[:index]
    #   index = page_params[:index]
    # else if (@chapter.pages.length)
    #   index =
    # end
    @page = @chapter.pages.create(page_params) #.merge(index: index))
    REDIS_CLIENT.publish('page/translation', JSON({action: 'add_page',
                                                   chapter: @page.chapter.id,
                                                   page: @page.id,
                                                   page_index: @page.index,
                                                   # page_progress: @page.translated_percent,
                                                   chapter_progress: @page.chapter.translated_percent}))
    # @page.page_texts.create
    api_render result: { page: @page }, code: 201
  end

  def update
    @page = Page.find(params[:id])
    unless @page
      raise ActionController::RoutingError.new('Not Found')
    end
    if page_params[:original_image]
      @page.original_image.attach(page_params[:original_image])
      REDIS_CLIENT.publish('page/translation', JSON({action: 'update_image',
                                                     chapter: @page.chapter.id,
                                                     page: @page.id,
                                                     image_url: url_for(@page.original_image.variant(resize: 'x800'))}))
      # REDIS_CLIENT.publish('page/image', JSON({ chapter: @page.chapter.id, page: @page.id }))
    end
    # unless page_params[:].nil?
    #   @page.translated_image.attach(page_params[:translated_image])
    # end
    render json: { page: @page }
  end

  def destroy
    @page = Page.find(params[:id])
    return api_render code: 404 unless @page

    @page.chapter.pages.where("index > ?", [@page.index]).each do |p|
      p.index -= 1
      p.save
    end

    REDIS_CLIENT.publish('page/translation', JSON({action: 'destroy_page',
                                                   chapter: @page.chapter.id,
                                                   page: @page.id,
                                                   page_index: @page.index,
                                                   # page_progress: @page.translated_percent,
                                                   chapter_progress: @page.chapter.translated_percent}))

    @page.destroy!

    api_render
  end

  private

  def page_params
    if params[:page]
      params.require(:page).permit([:original_image, :translated_image, :index])
    else
      {}
    end
  end
end
