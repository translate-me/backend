class PageTextsController < ApplicationController
  def create
    @page = Page.find(params[:page_id])
    return api_render code: 404 until @page
    @text = @page.page_texts.new
    @text.save
    REDIS_CLIENT.publish('page/translation', JSON({action: 'add', chapter: @text.page.chapter.id, page: @text.page.id, text: @text.id, text_index: @text.index, page_progress: @text.page.translated_percent, chapter_progress: @text.page.chapter.translated_percent}))
    render json: @text
  end

  def update
    @text = PageText.find(params[:id])
    return api_render code: 404 until @text
    if page_text_params[:index]
      @text.page.page_texts.each do |text|
        if text.index && text.index < @text.index && text.index >= page_text_params[:index].to_i
          text.update(index: text.index + 1)
        end
      end
    end
    # REDIS_CLIENT.publish('page/translation', 'AAAAAAAAAAAAAAAAAAAAAAAAA')

    @text.update(page_text_params)

    unless page_text_params[:is_translated].nil?
      puts 'UPDATE TRANSLAION', JSON({chapter: @text.page.chapter.id, page: @text.page.id, text: @text.id, translated: page_text_params[:is_translated]})
      REDIS_CLIENT.publish('page/translation', JSON({action: 'mark', chapter: @text.page.chapter.id, page: @text.page.id, text: @text.id, translated: page_text_params[:is_translated], page_progress: @text.page.translated_percent, chapter_progress: @text.page.chapter.translated_percent}))
    end

    api_render result: {page_text: @text}
  end

  def destroy
    @text = PageText.find(params[:id])
    return api_render code: 404 until @text

    @text.destroy
    api_render
  end

  private

  def page_text_params
    params.require(:page_text).permit(:is_translated, :index)
  end
end
