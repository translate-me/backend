class UsersController < ApplicationController
  def index
  end

  def show
    @user = User.find(params[:id])
    respond_to do |format|
      format.html
      format.json { api_render result: { user: @user }}
    end
  end

  def colors
    colors = {}
    User.all.each do |u|
      colors[u.id] = '#' + u.color
    end
    api_render result: {colors: colors}
  end

  def new
  end

  def create
    @user = User.new(user_params)
    render json: { errors: @user.errors } unless @user.save

    session[:user_id] = @user.id
    api_render result: { user: @user }, code: 201
  end

  def login
    @user = User.find_by_login(user_params[:login])
    return api_render code: 404 unless @user

    unless @user.check_password(user_params[:password])
      return api_render code: 403, errors: {password: ['wrong password']}
    end

    session[:user_id] = @user.id
    api_render
  end

  def edit
  end

  def update
    @user = User.find(params[:id])
    return api_render code: 404 unless @user

    @user.update(user_params)
    api_render result: { user: @user }, code: 200
  end

  def logout
    session[:user_id] = nil
    redirect_back fallback_location: '/mangas'
  end

  def destroy
  end

  private

  def user_params
    params.require(:user).permit(:login, :password, :color)
  end
end
