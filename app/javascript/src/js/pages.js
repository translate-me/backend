export function initAddAndRemove() {
    let chapter_wrapper = document.querySelector('#chapter-wrapper')
    let chapter_id = parseInt(chapter_wrapper.dataset.chapter||0)
    let chapter_page = parseInt(chapter_wrapper.dataset.chapterpage)
    let page_id = parseInt(chapter_wrapper.dataset.pageid||0)
    document.querySelector("#add-page").addEventListener('click', (e)=>{
        e.preventDefault()
        let pages = document.querySelectorAll('.page-texts')
        let body = new FormData()
        body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
        if (pages.length)
            body.append('page[index]', parseInt(pages[pages.length-1].dataset.pageindex)+1)
        fetch(`/chapters/${chapter_id}/pages`, {method: 'post', body: body}).then(async resp=> {
            resp = await resp.json()
            // reloadWithTurbolinks('#chapter-texts')
        })
    })

    document.querySelectorAll('.remove-manga').forEach(trigger => {
        trigger.addEventListener('click', (e) => {
            e.preventDefault()
            let body = new FormData()
            body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
            fetch(`/pages/${trigger.dataset.pageid}`, {method: 'delete', body: body}).then(async resp => {
                resp = await resp.json()
                reloadWithTurbolinks('#chapter-texts')
            })
        })
    })
}

function goToNextPad(el) {
    console.log('GTNP')
    let pads_list = Array.from(document.querySelectorAll('.pad'))
    if (pads_list.indexOf(el)===pads_list.length-1) {
        console.log('NEW PAD!')
        let addPadTriggers = document.querySelectorAll('#add-pad')
        addPad(addPadTriggers[addPadTriggers.length-1], ()=>goToNextPad(el))
        return
    }
    console.log(pads_list[pads_list.indexOf(el)+1].querySelector('.row'))
    pads[el.dataset.pad].focused = false
    pads[pads_list[pads_list.indexOf(el)+1].dataset.pad].focused = true
}

function extendPad(el) {
    pads[el.dataset.pad].extend(editor => {
        editor.on('keyup', (e) => {
            e.preventDefault()
            if (e.ctrlKey) {
                switch (e.key) {
                    case 'Enter':
                        console.log(editor.wrapper, e.path, e)
                        for (let elm of e.path) {
                            if (elm === editor.wrapper) {
                                console.log('Going~')
                                goToNextPad(editor.wrapper)
                            }
                        }
                        break
                }
            }
        })
    })
    if (el.dataset.pad.endsWith('original')) {
        pads[el.dataset.pad].extend((editor) => {
            editor.on('insert', ()=>el.parentElement.querySelector('.translate-suggestions').innerHTML = '')
            editor.on('delete', ()=>el.parentElement.querySelector('.translate-suggestions').innerHTML = '')
            let suggestions_wrapper = el.parentElement.querySelector('.translate-suggestions')
            document.addEventListener('click', (e) => {
                if (Array.from(e.path).indexOf(el)>=0 || Array.from(e.path).indexOf(suggestions_wrapper)>=0) {
                    el.parentElement.querySelector('.translate-suggestions').style.display = null
                } else {
                    el.parentElement.querySelector('.translate-suggestions').style.display = 'none'
                }
            })
            // editor.on('unfocused', ()=>{el.parentElement.querySelector('.translate-suggestions').style.display = 'none'})
            editor.socket.on('suggestions', suggestions => {
                suggestions_wrapper.innerHTML = ''
                for (let suggestion of suggestions) {
                    let sg = document.createElement('div')
                    sg.className = 'suggestion'
                    sg.innerHTML = `<span class="suggestion-name">${suggestion.name.title()}:</span><span class="suggestion-text">${suggestion.text}</span>`
                    sg.onclick = () => {
                        let translated_name = el.dataset.pad.replace('original', 'translated')
                        let data = []
                        for (let c of suggestion.text) {
                            data.push({key: c, owner: USER_ID})
                        }
                        console.log(data,pads[translated_name])
                        pads[translated_name].deleteAll()
                        pads[translated_name].multipleInsert(data, 0, 0, true)
                    }
                    suggestions_wrapper.appendChild(sg)
                }
            })
        })
    } else if (el.dataset.pad.endsWith('translated')) {
        pads[el.dataset.pad].extend(editor => {
            editor.on('insert', ()=>el.parentElement.querySelector('.translate-suggestions').innerHTML = '')
            editor.on('delete', ()=>el.parentElement.querySelector('.translate-suggestions').innerHTML = '')
        })
    }
}


let insertPad = (before, index, id) => {
    let newPad = document.createElement('div')
    newPad.className = 'pads-wrapper'
    newPad.dataset.textid = id
    newPad.innerHTML = `<div class="pad-info">
          <span class="pad-number">${index}</span>
          <span class="dnd-trigger">||</span>
          <a href="#" class="check-translated"><i class="fa fa-check"></i></a>
        </div>
        <div class="pads">
          <div class="pad pad-original no-left" data-pad="text-${id}_original" data-placeholder="Оригинал">

          </div>
          <div class="translate-suggestions">

            </div>
          <div class="pad pad-translated no-left" data-pad="text-${index}_translated" data-placeholder="Перевод">

          </div>
        </div>`
    if (document.querySelector(`[data-textid="${id}"]`)) return
    before.parentElement.insertBefore(newPad, before)
    newPad.querySelectorAll('.pad').forEach(el => {
        pads[el.dataset.pad] = new Editor(el)
        pads[el.dataset.pad].name = USER_ID
        extendPad(el)
    })
    return newPad
}

let addPad = (trigger, cb) => {
    let page_id = trigger.dataset.pageid
    // let chapter_wrapper = document.querySelector('#chapter-wrapper')
    let body = new FormData()
    body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
    fetch(`/pages/${page_id}/texts`, {method: 'post', body: body}).then(async (resp) => {
        let data = await resp.json()
        // let newPad = insertPad(trigger, data.index, data.id)
        setTimeout(() => {
            if(cb) {
                let pads = document.querySelectorAll('.pad')
                cb(pads[pads.length-1])
            }
        }, 500)
    })
}

function insertPage(index, id) {
    let newPage = document.createElement('div')
    newPage.className = 'page-texts'
    newPage.id = `page${id}`
    newPage.dataset.pageindex = index
    newPage.dataset.pageid = id
    newPage.innerHTML = `<div class="left">
            <div class="original-image active">
                <div class="dnd-image-loader">
                  <span>Кликни чтобы выбрать изображение</span>
                  <span>Или просто перетащи его сюда!</span>
                  <i class="fa fa-file-image"></i>
                </div>
              <form id="load-image" enctype="multipart/form-data" action="/pages/${id}" accept-charset="UTF-8" data-remote="true" method="post">
                <input name="utf8" type="hidden" value="✓"><input type="hidden" name="_method" value="patch"><input type="hidden" name="authenticity_token" value="${document.querySelector('meta[name="csrf-token"]').content}">
                <input id="load_original_image" class="load_image_input" type="file" name="page[original_image]">
            </form>
            </div>
          </div>
          <div class="page-data">
            <div class="page-index"><a href="#page${id}" class="page-title" data-turbolinks="false">Страница ${index}</a> <a href="#" class="remove-manga" data-pageid="${id}"><i class="fa fa-trash"></i></a></div>
            <div class="texts">
              <a href="#" id="add-pad" class="dnd-invisible" data-pageid="${id}"><i class="fa fa-plus"></i>Добавить надпись</a>
              <a href="#" id="trash-pad" class="dnd-trash"><i class="far fa-trash-alt"></i>Удалить</a>
            </div>
          </div>`
    let trigger = document.querySelector('#add-page')
    trigger.parentElement.insertBefore(newPage, trigger)
    initLeft(newPage)
    console.log(trigger, newPage)
}

export function initTexts() {
    let chapter_wrapper = document.querySelector('#chapter-wrapper')
    let chapter_id = parseInt(chapter_wrapper.dataset.chapter||0)
    let chapter_page = parseInt(chapter_wrapper.dataset.chapterpage)
    let page_id = parseInt(chapter_wrapper.dataset.pageid||0)
    window.pads = {}
    document.querySelectorAll('.pad').forEach(el => {
        pads[el.dataset.pad] = new Editor(el)
        pads[el.dataset.pad].name = USER_ID
        extendPad(el)
    })

    if (!window.socket_initiated) {

        window.socket.emit('listenChapter', document.querySelector('#chapter-wrapper').dataset.chapter)

        window.socket.on('translation', data => {
            if (chapter_id !== data.chapter) return
            let c_t = document.querySelector('[data-textid="132"] .check-translated')
            if (!c_t) return
            document.querySelector('#chapter-translation-progress').style.width = data.chapter_progress + "%"
            if (JSON.parse(data.translated))
                c_t.classList.add('active')
            else
                c_t.classList.remove('active')
        })

        window.socket.on('new_pad', data => {
            console.info(data)
            document.querySelector('#chapter-translation-progress').style.width = data.chapter_progress + "%"
            if (document.querySelector(`[data-textid="${data.text}"]`)) {
                console.log('ABORT')
                return
            }
            insertPad(document.querySelector(`#page${data.page} #add-pad`), data.text_index, data.text)
        })

        window.socket.on('new_page', data => {
            console.info(data)
            document.querySelector('#chapter-translation-progress').style.width = data.chapter_progress + "%"
            insertPage(data.page_index, data.page)
        })

        window.socket.on('update_image', data => {
            console.info(data)
            let wrapper = document.querySelector(`#page${data.page} .original-image`)
            let loadImage = wrapper.querySelector('#load-image')
            if (wrapper.querySelector('.chapter-image')) {
                wrapper.querySelector('.chapter-image').src = data.image_url
            } else {
                let img = document.createElement('img')
                img.src = data.image_url
                img.className = 'chapter-image'
                wrapper.appendChild(img)
                loadImage.style.display = 'none'
                loadImage.parentElement.parentElement.querySelector('.dnd-image-loader').style.display = 'none'
            }
        })

        window.socket.on('destroy_page', data => {
            console.info(data)
            document.querySelector('#chapter-translation-progress').style.width = data.chapter_progress + "%"
            document.querySelectorAll(`.page-texts`).forEach(page => {
                if (page.dataset.pageid + "" === data.page + "") {
                    page.remove()
                }
                if (parseInt(page.dataset.pageindex) > parseInt(data.page_index)) {
                    let newId = parseInt(page.dataset.pageid) - 1
                    let newIndex = parseInt(page.dataset.pageindex) - 1
                    page.dataset.pageid = newId
                    page.dataset.pageindex = newIndex
                    let title = page.querySelector('.page-title')
                    title.indexText = 'Страница' + newIndex + 1

                }
            })

        })

        window.socket_initiated = true
    }

    document.querySelectorAll('.pads-wrapper .check-translated').forEach(el => {
        el.addEventListener('click', (e) => {
            e.preventDefault()
            let body = new FormData()
            body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
            body.append('page_text[is_translated]', Array.from(el.classList).indexOf('active')<0)
            fetch(`/page_texts/${el.parentElement.parentElement.dataset.textid}`, {method: 'put', body: body}).then(async resp => {
                // reloadWithTurbolinks('#chapter-texts')
            })
        })
    })

    let addPadTriggers = document.querySelectorAll('#add-pad')

    if (!addPadTriggers.length) return

    addPadTriggers.forEach(trigger => trigger.addEventListener('click', (e)=>{
        e.preventDefault()
        addPad(trigger)
    }))
}

export function initLeft(target) {
    if (!target) target = document
    if (!target.querySelector('.left')) return
    target.querySelectorAll('.left').forEach(left => {
        left.querySelectorAll("#load_original_image, #load_translated_image").forEach(loadImage => {
            loadImage.addEventListener("change", (e) => {
                Rails.fire(loadImage.parentElement, 'submit')
            })
        })

        function preventDefaults(e) {
            e.preventDefault()
            e.stopPropagation()
        }

        let dropAreas = left.querySelectorAll(".dnd-image-loader, .chapter-image")
        dropAreas.forEach(dropArea => {
            ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
                dropArea.addEventListener(eventName, preventDefaults, false)
            })

            ;['dragenter', 'dragover'].forEach(name => {
                dropArea.addEventListener(name, () => {
                    dropArea.classList.add('active')
                })
            })
            ;['dragleave', 'drop'].forEach(name => {
                dropArea.addEventListener(name, () => {
                    dropArea.classList.remove('active')
                })
            })
            dropArea.addEventListener('drop', (e) => {
                let loadImage = dropArea.parentElement.querySelector('.load_image_input')
                loadImage.files = e.dataTransfer.files
            })

            dropArea.addEventListener('click', e => dropArea.parentElement.querySelector('.load_image_input').click())
        })
    })
}