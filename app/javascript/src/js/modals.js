export default function initModals() {
    let login_trigger = document.querySelector('#login-trigger')
    let login_form = document.querySelector("#login-form")
    if (login_trigger) {
        document.querySelectorAll('.modal-wrapper').forEach(wrapper => {
            wrapper.addEventListener('click', (e) => {
                // e.preventDefault();
                if (e.target === wrapper) wrapper.classList.remove("active")
            })
            wrapper.querySelector('button[type="submit"]').addEventListener('click', (e) => {
                Rails.fire(wrapper.querySelector('form'), 'submit')
            })
        })

        login_trigger.addEventListener('click',
            () => login_form.classList.add("active"))
        document.querySelector('#register-form form').addEventListener('ajax:success', ()=>reloadWithTurbolinks())
        document.querySelector('#register-form form').addEventListener('ajax:error', ()=>document.querySelector('#register-form .error').style.display = 'block')
        document.querySelector('#login-form form').addEventListener('ajax:success', ()=>reloadWithTurbolinks())
        document.querySelector('#login-form form').addEventListener('ajax:error', ()=>document.querySelector('#login-form .error').style.display = 'block')
        document.querySelector('#register-trigger').addEventListener('click',
            () => document.querySelector("#register-form").classList.add("active"))
    }

    let update_user_trigger = document.querySelector('#update-user-trigger')
    let update_user = document.querySelector('#update-user')
    if (update_user_trigger) {
        update_user.querySelector('form').addEventListener('ajax:success', ()=>reloadWithTurbolinks())
        update_user_trigger.addEventListener('click', (e) => {
            // e.preventDefault()
            update_user.classList.add("active")
            let onclick = e => {
                if (e.path.indexOf(update_user) < 0 && e.path.indexOf(update_user_trigger) < 0 && e.path.indexOf(document.querySelector('.jscolor-wrap')) < 0) {
                    update_user.classList.remove('active')
                    document.removeEventListener('click', onclick)
                }
            }
            document.addEventListener('click', onclick)
        })


    }
}