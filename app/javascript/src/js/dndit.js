// .dnd-item - els able do dnd
// .dnd-trigger - element click on which turns dnd on
// .dnd-trash - optional element, trashbox. Has visible class when dnd is in progress, and active class when item is above it.
// .dnd-invisible - elements which invisible when dnd is in progress

// .dnd-placeholder - placeholder class (you can style it with css!)

// t - where dndIt will search all elements
// getElByTrigger - function which should return element with .dnd-item

export default function dndIt(t, getElByTrigger, callback, trashCallback, debug) {
    t = t || document

    if (!getElByTrigger)
        getElByTrigger = (trigger) => {
            let el = null
            let a = trigger
            while (a && !el) {
                a = a.parentNode;
                if (a.classList && a.classList.contains('dnd-item')) el = a
            }
            return el
        }

    function log(...data) {
        if (debug) console.log(...data)
    }

    t.querySelectorAll('.dnd-trigger').forEach(trigger => {
        if (t.querySelectorAll('.dnd-trigger').length<2) return
        let el = getElByTrigger(trigger)
        trigger.addEventListener('mousedown', (e) => {
            el.classList.add('dragging')
            t.querySelectorAll('.dnd-invisible').forEach(el => el.style.display = 'none')
            let elStyle = getComputedStyle(el)

            let box = el.getBoundingClientRect()
            el.style.width = box.width + "px"
            el.style.height = box.height + "px"

            let trash = el.parentElement.querySelector('.dnd-trash')
            if (trash) {
                trash.classList.add('visible')
                trash.style.width = box.width + "px"
                trash.style.height = parseInt(elStyle.height) * 1.5 + "px"
            }

            el.style.position = 'absolute'
            el.style.zIndex = '100500'
            el.style.width = box.width + "px"
            el.style.height = box.height + "px"

            let offsetLeft = e.x - box.left
            let offsetTop = e.y - box.top

            el.style.left = e.x - offsetLeft + "px"
            el.style.top = e.y - offsetTop + "px"

            log(e)

            let placeholder = document.createElement('div')
            placeholder.className = 'dnd-placeholder'
            placeholder.style.width = box.width + "px"
            placeholder.style.height = box.height + "px"
            placeholder.style.margin = elStyle.margin
            el.parentElement.insertBefore(placeholder, el)

            let oldItemToMove = null
            let oldDirection = null

            document.body.append(el)

            let onMouseMove = (e) => {
                e.preventDefault()
                el.style.left = e.x - offsetLeft + "px"
                el.style.top = e.y - offsetTop + "px"

                let itemToMove = null
                let direction = 'up'

                let els = Array.from(el.parentElement.querySelectorAll('.dnd-item:not(.dragging)'))
                for (let l_el of els) {
                    let b = l_el.getBoundingClientRect()
                    let style = getComputedStyle(l_el)
                    let marginBottom = parseInt(style.marginBottom)
                    if (b.top < e.y && b.top + b.height + marginBottom > e.y) {
                        itemToMove = l_el
                        let diff = b.y + b.height - e.y
                        // log(diff, b.height/2, e.y, b,l_el)
                        if (diff < b.height / 2) direction = 'down'
                        break
                    }
                }

                if (trash) {
                    let b = trash.getBoundingClientRect()
                    if (e.x > b.x && e.x < b.x + b.width && e.y > b.y && e.y < b.y + b.height) {
                        oldDirection = null
                        oldItemToMove = null
                        trash.classList.add('active')
                        let placeholder = t.querySelector('.dnd-placeholder')
                        if (placeholder) placeholder.remove()
                    } else {
                        trash.classList.remove('active')
                    }
                }

                if (itemToMove && (itemToMove !== oldItemToMove || direction !== oldDirection)) {
                    oldItemToMove = itemToMove
                    oldDirection = direction
                    log(itemToMove, direction)
                    let placeholder = t.querySelector('.dnd-placeholder')
                    if (!placeholder) {
                        placeholder = document.createElement('div')
                        placeholder.className = 'dnd-placeholder'
                    }
                    placeholder.style.width = elStyle.width
                    placeholder.style.height = elStyle.height
                    placeholder.style.margin = elStyle.margin

                    if (direction === 'up') {
                        itemToMove.parentElement.insertBefore(placeholder, itemToMove)
                    } else if (direction === 'down' && itemToMove.nextElementSibling) {
                        itemToMove.parentElement.insertBefore(placeholder, itemToMove.nextElementSibling)
                    } else {
                        itemToMove.parentElement.appendChild(placeholder)
                    }
                }

            }

            let onMouseUp = (e) => {
                t.querySelectorAll('.dnd-invisible').forEach(el => el.style.display = null)
                document.removeEventListener('mousemove', onMouseMove)
                document.removeEventListener('mouseup', onMouseUp)

                let placeholder = t.querySelector('.dnd-placeholder')
                if (placeholder) {
                    placeholder.parentElement.insertBefore(el, placeholder)
                    placeholder.remove()
                }

                if (trash) {
                    trash.classList.remove('visible')
                    if (trash.classList.contains('active')) {
                        if (trashCallback)
                            trashCallback(el)
                        else
                            el.remove()
                        return
                    }
                }
                el.style.position = null
                el.style.zIndex = null
                el.style.left = null
                el.style.top = null
                el.style.width = null
                el.style.height = null
                el.classList.remove('dragging')

                if (callback) callback(el)
            }

            document.addEventListener('mousemove', onMouseMove)

            document.addEventListener('mouseup', onMouseUp)
        })
    })
}