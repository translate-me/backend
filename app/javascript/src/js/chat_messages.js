// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

export default function initChat() {
    document.querySelector('#chat-bottom').addEventListener('ajax:success', (e) => {
        e.preventDefault()
        document.querySelector('#chat-input').value = ''
        chat_messages.scrollTo.apply(chat_messages, [0, chat_messages.scrollHeight])
    })
    if (window.chat_socket) return
    let chat_messages = document.querySelector("#chat-messages")
    chat_messages.scrollTo.apply(chat_messages, [0, chat_messages.scrollHeight])
    window.chat_socket = io(SOCKETS_URL)
    chat_socket.emit('listenChapter', document.querySelector('#chapter-wrapper').dataset.chapter)
    chat_socket.on('message', message => {
        console.log(message)
        let node = document.createElement('div')
        node.className = 'message'
        let date = new Date(message.created_at)
        node.innerHTML = `<span class="datetime">[${date.getHours()}:${date.getMinutes()<10? "0"+date.getMinutes() : date.getMinutes()}]</span>
            <a href="#" style="color: #${message.user_color};">${message.user_login}</a>:
            <span class="text">${message.text}</span>`
        let chat_messages = document.querySelector("#chat-messages")
        let scroll = false
        if (Math.round(chat_messages.scrollTop + chat_messages.getBoundingClientRect().height) == chat_messages.scrollHeight)
            scroll = true
        chat_messages.appendChild(node)
        if (scroll) chat_messages.scrollTo.apply(chat_messages, [0, chat_messages.scrollHeight])
    })
}