export default class Editor {
    constructor(wrapper, data) {
        this.debug = false

        this.listeners = {}

        this.wrapper = wrapper
        let wrapper_data = wrapper.querySelector("#data")
        if (wrapper_data && wrapper_data.innerText) {
            try {
                // console.log(wrapper.innerText)
                data = JSON.parse(wrapper_data.innerText.replace(/\n/gm, "\\n"))
                // wrapper.innerText = ""
            } catch(e) {
                console.info(wrapper_data.innerText,wrapper_data.innerHTML)
                console.info(wrapper_data.innerText.replace(/\n/gm, "\\n"))
                console.error(e)
            }
        }


        this.padName = this.wrapper.dataset.pad
        this.el = wrapper.appendChild(document.createElement('div'))
        this.el.className = 'fakeArea'
        this.caret_index = 0
        this.caret_row = 0
        this.focused = false
        this.init()
        this.textarea = wrapper.appendChild(document.createElement('textarea'))
        this.textarea.className = 'hiddenTextarea'
        this.socket = window.socket
        this.socket.emit('joinPad', this.padName, this.wrapper.id)
        this.socket.on('insert', (padName, key, r, i, name) => {
            if (padName!==this.padName) return
            this._insert(key, r,i,name)
        })
        this.socket.on('delete', (padName, r, i, name) => {
            if (padName!==this.padName) return
            this.delete(r,i,name)
        })
        this.placeholder = wrapper.dataset.placeholder
        if (data) {
            this.multipleInsert(data, 0, 0)
        }
        this.name = wrapper.id

        this.color = USER_COLOR

        this.sel = null
        this.dir = null
        this.lastX = 0
        this.clickStart = 0
        this.selInProcess = false
    }

    getSelectedPoints() {
        if (!this.sel) return
        console.log(this.sel)

        let els = Array.from(this.el.querySelectorAll('.cell, .row-start, .caret-wrapper'))

        let isElIn = (x,y,i) => {
            let box = els[i].getBoundingClientRect()
            if (box.left <= x && box.top <= y && box.left + box.width >= x && box.top + box.height >= y) {
             return els[i]
            }
            if (this.dir===1) {
                if (box.left <= x && box.top <= y && box.left + box.width >= x && box.top + box.height >= y) {
                    return els[i]
                } else if (box.left <= x && box.top <= y && box.left + box.width >= x && box.top + box.height >= y) {
                    return els[i-1]
                }
            }
            if (this.dir===-1) {
                if (box.left <= x && box.top <= y && box.left + box.width/2 >= x && box.top + box.height >= y) {
                    return els[i]
                } else if (box.left <= x && box.top <= y && box.left + box.width >= x && box.top + box.height >= y) {
                    return els[i+1]
                }
            }
            return false
        }

        let points = [null, null]

        for (let i=0; i<els.length; i++) {
            let el = els[i]
            let zero = isElIn(this.sel[0][0], this.sel[0][1], i)
            if (zero) points[0] = zero
            let one = isElIn(this.sel[1][0],this.sel[1][1], i)
            if (one) points[1] = one
        }
        return {points: points, els: els}
    }

    getSelected() {
        if (!this.sel) return
        let { points, els } = this.getSelectedPoints()

        let indexes = [els.indexOf(points[0]), els.indexOf(points[1])]
        return els.slice(Math.min(...indexes), Math.max(...indexes)+1)
    }

    updateSelection() {
        let cells = Array.from(this.el.querySelectorAll('.cell'))
        if (!this.sel || this.sel.length<2 || !this.sel[0] || !this.sel[1]) {
            console.log('No selected')
            for (let cell of cells) {
                cell.classList.remove('selected')
            }
            return
        }
        let selected = Array.from(this.getSelected())
        if (!selected && !selected.length) {
            console.log('No selected')
            for (let cell of cells) {
                cell.classList.remove('selected')
            }
        }
        console.log('SELECTED!')
        for (let cell of cells) {
            if (selected.indexOf(cell)>=0)
                cell.classList.add('selected')
            else
                cell.classList.remove('selected')
        }
        let zero_pos = this.getCellPosition(selected[0])
        let one_pos = this.getCellPosition(selected[1])
        if (zero_pos[1]<one_pos[1]) {
            let pos = this.getCellPosition(selected[selected.length-1])
            this.caret_row = pos[0]
            this.caret_index = pos[1]+1
            this.replaceCaret()
        }
        // if (this.dir===-1) {
        //     let pos = this.getCellPosition(selected[0])
        //     this.caret_row = pos[0]
        //     this.caret_index = pos[1]
        //     this.replaceCaret()
        // } else if (this.dir===1) {
        //     let pos = this.getCellPosition(selected[selected.length-1])
        //     this.caret_row = pos[0]
        //     this.caret_index = pos[1]+1
        //     this.replaceCaret()
        // }
    }

    copy() {
        let els = Array.from(this.el.querySelectorAll('.cell, .row-start'))
        let st = this.el.querySelectorAll('.cell.selected')
        let selected = els.slice(els.indexOf(st[0]),els.indexOf(st[st.length-1])+1)

        if (!selected && !selected.length) return

        let sel = window.getSelection()


        let s = ""
        for (let cell of selected) {
            s += cell.innerText==='\\n'? '\n' : cell.innerText
        }

        let t = document.createElement('div')
        t.innerText = s
        document.body.appendChild(t)
        const range = document.createRange();
        range.selectNodeContents(t);
        sel.removeAllRanges();
        sel.addRange(range);
        document.execCommand('copy');
        sel.removeAllRanges();
        t.remove()
        this.sel = [null, null]
        this.updateSelection()
    }

    on(event, func) {
        if (event in this.listeners){
            this.listeners[event].push(func)
        } else {
            this.listeners[event] = [func]
        }
    }

    off(event, func) {
        if (!(event in this.listeners)) return
        this.listeners.splice(this.listeners[event].indexOf(func),1)
    }

    emit(event, ...data) {
        if (!(event in this.listeners)) return
        this.listeners[event].forEach(func => func(...data))
    }

    deleteAll() {
        this.caret_row = 0
        this.caret_index = 0
        this.replaceCaret()
        while (this.el.querySelectorAll('.cell').length) {
            this.delete_key()
        }
    }

    extend(func) {
        this.log('EXTENDED!')
        func(this)
    }

    set placeholder(value) {
        this._placeholder = value
        let pd = document.createElement('span')
        pd.className = 'placeholder'
        pd.innerText = value
        this.el.insertBefore(pd, this.el.querySelector('*'))
    }

    get placeholder() {
        return this._placeholder
    }

    showPlaceholder() {
        let ph = this.el.querySelector('.placeholder')
        if (!ph) return
        ph.style.display = 'inline-block'
    }

    hidePlaceholder() {
        let ph = this.el.querySelector('.placeholder')
        if (!ph) return
        ph.style.display = 'none'
    }

    set name(value) {
        document.querySelectorAll(`.owner-${this.name}`).forEach(elem=>{elem.classList.remove(`owner-${this.name}`);elem.classList.add(`owner-${value}`)})
        this.socket.emit('setName', value)
        this._name = value
        // let css = document.createElement('style')
        // css.innerHTML = `.owner-${this.name} {background-color: ${this.color};}`
        // document.body.appendChild(css)
    }

    get name() {
        return this._name
    }

    set color(value) {
        this._color = value
    }

    updateOwnerColor(owner) {
        if (owner in COLORS) {
            this.log('SET')
            this.el.querySelectorAll(`.cell.owner-${owner}`).forEach(cell => {
                cell.style.backgroundColor = COLORS[owner]
            })
        } else {
            this.log('WILL LOAD')
            console.log(COLORS,owner)
            fetch('/users/'+owner, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then(async resp=>{
                let data = await resp.json()
                this.log(data)
                let color = data.result.user.color
                if (!color.startsWith('#')) color = '#'+color
                COLORS[owner] = color
                this.el.querySelectorAll(`.cell.owner-${owner}`).forEach(cell => {
                    cell.style.backgroundColor = COLORS[owner]
                })
            })
        }
    }

    get color() {
        return this._color
    }

    log() {
        if (!this.debug) return
        console.log.call(this, arguments)
    }

    get focused() {
        return Array.from(this.wrapper.classList).indexOf('focused')>=0
    }

    set focused(val) {
        this.log(this.padName,val)
        this.wrapper.classList.remove('focused')
        if (val) {
            if (window.getSelection().isCollapsed)
                this.textarea.focus()
            this.log('focused')
            this.wrapper.classList.add('focused')
            this.wrapper.style.borderColor = this.color
            this.emit('focused')
        } else {
            this.log('unfocused')
            this.wrapper.style.borderColor = null
            this.emit('unfocused')
        }
    }

    init() {
        this.el.innerHTML = `<div class='row' id="row-0"><span class="line-num">1</span>
    <div class="caret-wrapper">
        <div class="caret"></div>
    </div>
</div>`
        function isTargetInParents(target, el) {
            if (el.parentElement===target) return true
            if (!el.parentElement)
                return false
            return isTargetInParents(target, el.parentElement)
        }
        document.addEventListener('mousedown', (e) => {
            this.clickStart = new Date()
            if (isTargetInParents(this.wrapper, e.target) || e.target===this.wrapper){
                this.focused = true
                this.sel = [[e.pageX,e.pageY]]
                this.lastX = e.pageX
                this.selInProcess = true
                let onmove = (e) => {
                    this.sel[1] = [e.pageX, e.pageY]
                    this.dir = this.lastX<e.pageX? 1 : -1
                    this.lastX = e.pageX
                    this.log(this.sel)
                    this.updateSelection()
                }
                let onup = (e) => {
                    this.selInProcess = false
                    this.sel[1] = [e.pageX, e.pageY]
                    if (new Date() - this.clickStart < 200) {
                        this.sel = [null, null]
                        if (Array.from(e.target.classList).indexOf('cell')>=0) {
                            this.caret_row = parseInt(e.target.parentElement.id.replace('row-', ''))
                            let cell_box = e.target.getBoundingClientRect()
                            let add = 0
                            if (e.pageX-cell_box.x > cell_box.width/2) add =1
                            this.caret_index = Array.from(e.target.parentElement.querySelectorAll('.cell')).indexOf(e.target) + add
                            this.replaceCaret()
                        }
                        if (Array.from(e.target.classList).indexOf('row')>=0) {
                            this.caret_row = parseInt(e.target.id.replace('row-', ''))
                            this.caret_index = Array.from(e.target.querySelectorAll('.cell')).length
                            this.replaceCaret()
                        }
                        this.focused = true
                        if (window.getSelection().isCollapsed)
                            this.textarea.focus()
                    }
                    this.updateSelection()
                    document.removeEventListener('mouseup', onup)
                    document.removeEventListener('mousemove', onmove)
                }
                document.addEventListener('mousemove', onmove)
                document.addEventListener('mouseup', onup)
            } else {
                this.focused = false
                if (!this.sel || this.sel[0] !== null) {
                    this.sel = [null, null]
                    this.updateSelection()
                }
            }
        })

        document.addEventListener('keyup', (e)=> {
            if (this.focused) {
                this.emit('keyup',e)
                let paste_data = ''
                switch(e.key) {
                    case 'v':
                        if (!e.ctrlKey) break
                        paste_data = this.textarea.value
                        for (let i=0; i<paste_data.length; i++) {
                            this.insert(paste_data[i], this.caret_row, this.caret_index)
                        }
                        break
                    case 'м':
                        if (!e.ctrlKey) break
                        paste_data = this.textarea.value
                        for (let i=0; i<paste_data.length; i++) {
                            this.insert(paste_data[i], this.caret_row, this.caret_index)
                        }
                }
                this.textarea.value = ''
            }
        })

        document.addEventListener('keydown', (e)=>{
            if (this.focused && !e.ctrlKey) {
                this.log(e)
                if (e.key.match(/F\d/))
                    return
                this.log(e.key, e.key.length, e)
                if (e.key.length < 2) {
                    this.log("INSERT!")
                    this.insert(e.key, this.caret_row, this.caret_index)
                } else {
                    switch (e.key) {
                        case 'Control':
                            break
                        case 'Enter':
                            this.insert('\n', this.caret_row, this.caret_index)
                            this.replaceCaret()
                            break
                        case 'Backspace':
                            this.backspace()
                            break
                        case 'Delete':
                            this.delete_key()
                            break
                        case 'ArrowLeft':
                            this.moveHorizontaly(-1)
                            break
                        case 'ArrowRight':
                            this.moveHorizontaly(1)
                            break
                        case 'ArrowUp':
                            this.moveVerticaly(-1)
                            break
                        case 'ArrowDown':
                            this.moveVerticaly(1)
                            break
                        case 'End':
                            this.toRowEnd()
                            break
                        case 'Home':
                            this.toRowStart()
                            break
                        case 'Tab':
                            if (e.shiftKey) break
                            this._insert('\xa0\xa0\xa0\xa0', this.caret_row, this.caret_index, this.name)
                            break
                        case 'Shift':
                            break
                        default:
                            // this.insert(e.key, this.caret_row, this.caret_index)
                    }
                }
            } else if(this.focused && e.ctrlKey) {
                switch(e.key) {
                    case 'Backspace':
                        this.backspace(true)
                        break
                    case 'Delete':
                        this.delete_key(true)
                        break
                    case 'ArrowLeft':
                        this.ctrlLeft()
                        break
                    case 'ArrowRight':
                        this.ctrlRight()
                        break
                    case 'c':
                        this.copy()
                        break
                    case 'a':
                        this.selectAll()
                        break
                }
            }
        })
    }

    selectAll() {
        this.el.querySelectorAll('.cell:not(.selected)').forEach(cell=>cell.classList.add('selected'))
    }

    multipleInsert(data, r,i, send) {
        let tmp_index = i
        let tmp_row = r
        for (let y=0; y<data.length; y++) {
            let l = data[y]
            if (l.key==" ") l.key = '\u00A0'
            if (send) this.socket.emit('insert', this.padName, l.key, tmp_row, tmp_index)
            this._insert(l.key, tmp_row, tmp_index, l.owner)
            tmp_index += 1
            if (l.key==='\n'){
                tmp_row += 1
                tmp_index = 0
            }
        }
    }

    insert(key,r,i) {
        this.log(this.name, 'insert', key)
        if (key==='\n') {
            this.caret_row += 1
            this.caret_index = 0
        } else {
            this.caret_index += 1
        }
        if (key===" ") {
            this._insert('\u00A0',r,i, this.name)
            this.socket.emit('insert', this.padName, '\u00A0',r,i)
            this.replaceCaret()
            return
        }
        this.socket.emit('insert', this.padName, key,r,i)
        this._insert(key,r,i, this.name)
        this.replaceCaret()

    }

    delete_key(ctrl) {
        let selected = Array.from(this.el.querySelectorAll('.cell.selected'))
        let firstPosition = null
        if (selected && selected.length) {
            for (let cell of selected) {
                let pos = this.getCellPosition(cell)
                if (!firstPosition) firstPosition = pos
                this.delete(...pos)
            }
            this.caret_row = firstPosition[0]
            this.caret_index = firstPosition[1]
            this.replaceCaret()
            return
        }
        if (!this.el.querySelector('.caret-wrapper').nextElementSibling && this.el.querySelectorAll('.row')>1) return
        if (ctrl && this.el.querySelector('.caret-wrapper').nextElementSibling) {
            let cells = Array.from(this.el.querySelectorAll(`#row-${this.caret_row} .cell`)).slice(this.caret_index)
            this.log("DELETE CELLS", cells)
            for (let i=0; i<cells.length;i++) {
                let cell = cells[i]
                this.log(cell)
                if (cell.innerText.match(/[^а-яА-Я\w]/)) {
                    this.log("NONWORD")
                    if (i===0) {
                        this.delete(this.caret_row, this.caret_index)
                        this.emitDelete(this.caret_row, this.caret_index)
                    }
                    break
                }
                this.delete(this.caret_row, this.caret_index)
                this.emitDelete(this.caret_row, this.caret_index)
            }
            return
        }
        this.delete(this.caret_row, this.caret_index)
        this.emitDelete(this.caret_row, this.caret_index)
        this.replaceCaret()
    }

    getCellPosition(cell) {
        if (!cell) return [0,0]
        let row = cell.parentElement
        return [parseInt(row.id.replace('row-', '')), Array.from(row.querySelectorAll('.cell')).indexOf(cell)]
    }

    backspace(ctrl) {
        let selected = Array.from(this.el.querySelectorAll('.cell.selected'))
        let firstPosition = null
        if (selected && selected.length) {
            for (let cell of selected) {
                let pos = this.getCellPosition(cell)
                if (!firstPosition) {
                    firstPosition = pos
                    this.caret_row = firstPosition[0]
                    this.caret_index = firstPosition[1]
                    this.replaceCaret()
                }
                this.delete(...pos)
                this.emitDelete(...pos)
                let row = this.el.querySelector(`#row-${pos[0]}`)
                if (row && !row.querySelectorAll('.cell').length) {
                    this.delete(pos[0], 0)
                    this.emitDelete(pos[0], 0)
                }
            }
            return
        }

        if (ctrl) {
            let cells = Array.from(this.el.querySelectorAll(`#row-${this.caret_row} .cell`)).slice(0, this.caret_index)
            this.log(cells)
            cells = cells.reverse()
            for (let i=0; i<cells.length;i++) {
                let cell = cells[i]
                if (cell.innerText.match(/[^а-яА-Я\w]/)) {
                    if (i===0) {
                        this.delete(this.caret_row, cells.length-1)
                        this.emitDelete(this.caret_row, cells.length-1)
                    }
                    break
                }
                this.delete(this.caret_row, cells.length-1-i)
                this.emitDelete(this.caret_row, cells.length-1-i)
            }
            return
        }
        if (this.caret_index===0 && this.caret_row===0)
            return
        let new_index = this.caret_index
        let new_row = this.caret_row
        if (this.caret_index===0 && this.el.querySelector(`#row-${this.caret_row-1}`)) {
            new_index = this.el.querySelectorAll(`#row-${this.caret_row-1} .cell`).length
            new_row -= 1
        } else {
            new_index -= 1
        }
        this.emitDelete(this.caret_row, this.caret_index-1)
        this.delete(this.caret_row, this.caret_index-1)
        this.caret_row = new_row
        this.caret_index = new_index
        this.replaceCaret()
    }

    emitDelete(r,i) {
        this.socket.emit('delete', this.padName, r,i, this.name)
    }

    toRowStart() {
        this.caret_index = 0
        this.replaceCaret()
    }

    toRowEnd() {
        this.caret_index = this.el.querySelectorAll(`#row-${this.caret_row} .cell`).length
        this.replaceCaret()
    }

    moveVerticaly(k) {
        if (k<0 && this.caret_row===0)
            return
        if (this.caret_row+k>=this.el.querySelectorAll('.row').length)
            return
        this.caret_row += k
        let cells = this.el.querySelectorAll(`#row-${this.caret_row} .cell`)
        this.log(this.caret_row, k, cells.length, cells)
        if (this.caret_index>cells.length)
            this.caret_index = cells.length
        this.replaceCaret()
    }

    ctrlLeft() {
        if (!this.caret_index) return this.moveHorizontaly(-1)
        let els = Array.from(this.el.querySelectorAll(`#row-${this.caret_row} .cell`)).slice(0, this.caret_index).reverse()
        let newIndex = 0
        for (let i=0; i<els.length; i++) {
            let cell = els[i]
            if (cell.innerText.match(/[^а-яА-Я\w]/)) {
                newIndex = i? els.length-1-els.indexOf(cell)+1 : this.caret_index-1
                break
            }
        }
        this.caret_index = newIndex
        this.replaceCaret()
    }

    ctrlRight() {
        if (!this.el.querySelector('.caret-wrapper').nextElementSibling) return this.moveHorizontaly(1)
        let els = Array.from(this.el.querySelectorAll(`#row-${this.caret_row} .cell`)).slice(this.caret_index)
        let newIndex = this.caret_index+els.length
        for (let i=0; i<els.length; i++) {
            let cell = els[i]
            if (cell.innerText.match(/[^а-яА-Я\w]/)) {
                newIndex = i? this.caret_index + (els.indexOf(cell) || 1) : this.caret_index+1
                break
            }
        }
        this.caret_index = newIndex
        this.replaceCaret()
    }

    moveHorizontaly(k) {
        if (this.caret_index+k<0 && this.caret_row===0) {
            this.log('No left', this.caret_index)
            return
        }
        if (this.caret_index+k>this.el.querySelectorAll(`#row-${this.caret_row} .cell`).length && this.el.querySelectorAll('.row').length-1===this.caret_row) {
            this.log('No right', this.caret_index)
            return
        }
        if (this.caret_index+k<0) {
            this.caret_row -= 1
            this.caret_index = this.el.querySelectorAll(`#row-${this.caret_row} .cell`).length
            this.replaceCaret()
            return
        }
        if (this.caret_index+k>this.el.querySelectorAll(`#row-${this.caret_row} .cell`).length) {
            this.caret_row += 1
            this.caret_index = 0
            this.replaceCaret()
            return
        }

        this.caret_index += k
        this.replaceCaret()
    }

    delete = (r,i,owner) => {
        if (i < -1) {
            this.log('i<1')
            return
        }
        this.emit('delete', r,i)
        let row = this.el.querySelector(`#row-${r}`)
        let cells = row.querySelectorAll('.cell')
        let nextRow = document.querySelector(`#row-${r+1}`)
        if (i===cells.length) {
            this.log('i===cells.length')
            if (!nextRow) return
            this.log(nextRow)
            row.append(...nextRow.querySelectorAll('.cell'))
            this.removeRow(r+1)
            this.replaceCaret()
            return
        }
        if (i===-1 && r!==0) {
            this.log(i===-1 && r!==0)
            let old_cells = this.el.querySelectorAll(`#row-${r-1} .cell`)
            this.el.querySelector(`#row-${r-1}`).append(...cells)
            this.removeRow(r)
            if (owner!==this.name && r>=this.caret_row && r===this.caret_row) {
                if (r===this.caret_row) {
                    this.caret_index += old_cells.length
                }
                this.caret_row -= 1

                this.replaceCaret()
            }
            return
        }

        this.log(this.name, row.querySelectorAll('.cell'), row.querySelectorAll('.cell')[i], i)
        this.log(this.name, 'delete', `R: ${r}, I: ${i}`, row.querySelectorAll('.cell')[i])

        row.querySelectorAll('.cell')[i].remove()

        if (!this.el.querySelectorAll('.cell').length) {
            this.showPlaceholder()
        }

        if (owner!==this.name && r===this.caret_row&&i<this.caret_index) {
            let new_index = this.caret_index
            new_index -= 1
            this.caret_index = new_index
            this.replaceCaret()
        }
    }

    removeRow(r) {
        let row = this.el.querySelector(`#row-${r}`)
        row.remove()
        let rows = Array.from(this.el.querySelectorAll('.row'))
        for (let y=0; y<rows.length; y++) {
            if (parseInt(rows[y].id.match(/row-(\d+)/)[1])<=r) continue
            let row = rows[y]
            let new_id = parseInt(row.id.match(/row-(\d+)/)[1])-1
            row.id = `row-${new_id}`
            row.querySelector('.line-num').innerHTML = new_id+1
        }
    }

    _insert(key, r, i, owner) {
        this.emit('insert', key,r,i)
        this.hidePlaceholder()
        // this.log(this.name, '_insert', key)
        if (key==='\n') {
            let cells = Array.from(this.el.querySelectorAll(`#row-${r} .cell`)).slice(i)
            this.newRow(r, cells)
            if (owner!==this.name && r>=this.caret_row&&this.caret_index>i) {
                if (this.caret_row===r)
                    this.caret_index -= i
                this.caret_row += 1
                // this.log(this.name, this.caret_row, this.caret_index)
                this.replaceCaret()
            }
            return
        }
        let row = this.el.querySelector(`#row-${r}`)
        let cells = row.querySelectorAll('.cell')
        // this.log(key,r,i,cells)
        let cell = document.createElement('span')
        cell.style.backgroundColor = owner && COLORS[owner] ? COLORS[owner] : USER_COLOR
        cell.className = 'cell owner-'+owner
        cell.innerText = key
        if (cells.length===i) {
            row.appendChild(cell)
        } else {
            row.insertBefore(cell, cells[i])
        }
        if (owner!==this.name && r===this.caret_row && i<this.caret_index) {
            this.caret_index += 1
            this.replaceCaret()
        }
        if (!COLORS[owner]) {
            this.updateOwnerColor(owner)
        }
    }

    replaceCaret() {
        this.log('replace',this.name, this.caret_row, this.caret_index)
        let caret = this.el.querySelector('.caret-wrapper')
        if (!caret) {
            caret = document.createElement('div')
            caret.className = 'caret-wrapper'
            caret.innerHTML = `<div class='caret'></div>`
        }
        let row = this.el.querySelector(`#row-${this.caret_row}`)
        let cells = row.querySelectorAll('.cell')
        if (cells.length===this.caret_index) {
            row.appendChild(caret)
            return
        }
        row.insertBefore(caret, cells[this.caret_index])
    }

    newRow(r, cells=[]) {
        let new_row = document.createElement('div')
        new_row.className = 'row'
        new_row.id = `row-${r+1}`
        new_row.innerHTML = `<span class="line-num">${r+2}</span><span class="row-start">\\n</span>`
        let row_before = this.el.querySelector(`#row-${r+1}`)
        if (row_before) {
            let rows = Array.from(this.el.querySelectorAll('.row'))
            for (let i = 0; i < rows.length; i++) {
                if (i < parseInt(row_before.id.match(/row-(\d+)/)[1])) continue
                let row = rows[i]
                let new_id = parseInt(row.id.match(/row-(\d+)/)[1]) + 1
                row.id = `row-${new_id}`
                row.querySelector('.line-num').innerHTML = new_id + 1
            }
        }
        if (r===this.el.querySelectorAll('.row').length-1) {
            this.el.appendChild(new_row)
        } else {
            this.el.insertBefore(new_row, row_before)
        }
        if (cells.length)
            this.el.querySelector(`#row-${r+1}`).append(...cells)
    }
}