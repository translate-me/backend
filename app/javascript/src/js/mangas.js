export function initChapterList() {
    document.querySelector('#add-chapter').addEventListener('ajax:success', (e) => {
        e.preventDefault()
        reloadWithTurbolinks()
    })
    document.querySelectorAll('.edit-chapter').forEach(trigger => {
        trigger.addEventListener('click', (e) => {
            e.preventDefault()

            let title = trigger.dataset.title
            let id = trigger.dataset.id
            let number = trigger.dataset.number
            let wrapper = trigger.parentElement.parentElement
            wrapper.innerHTML = `<form id="update-chapter" action="/chapters/${id}" accept-charset="UTF-8" data-remote="true" method="patch">
      <input type="number" name="chapter[number]" id="chapter-number" placeholder="Номер главы" step="0.1" min="0" value="${number}">
      <input type="text" name="chapter[title]" id="chapter-title" placeholder="Название главы" value="${title}">
      <button type="submit">Сохранить</button>
</form>`
            wrapper.querySelector('button[type="submit"]').addEventListener('click', (e) => {
                e.preventDefault()
                let body = new FormData()
                body.append('chapter[title]', wrapper.querySelector('#chapter-title').value)
                body.append('chapter[number]', wrapper.querySelector('#chapter-number').value)
                body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
                fetch(`/chapters/${trigger.dataset.id}`, {method: 'put', body: body}).then(async resp => {
                    resp = await resp.json()
                    reloadWithTurbolinks()
                })
            })
        })
    })
    document.querySelectorAll('.remove-chapter').forEach(trigger => {
        trigger.addEventListener('click', (e) => {
            e.preventDefault()
            let body = new FormData()
            body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
            fetch(`/chapters/${trigger.dataset.id}`, {method: 'delete', body: body}).then(async resp => {
                resp = await resp.json()
                reloadWithTurbolinks()
            })
        })
    })
}

export function initMangaForm() {
    let edit_manga = document.querySelector('.edit_manga, .add-manga')
    if (!edit_manga) return
    edit_manga.addEventListener('ajax:success', (e) => {
        e.preventDefault()
        if (e.detail[0].result.manga) {
            Turbolinks.visit(`/mangas/${e.detail[0].result.manga.id}`)
        } else {
            reloadWithTurbolinks()
        }
    })

    let edit_manga_trigger = document.querySelector('#edit-manga-trigger')
    let delete_manga_trigger = document.querySelector('#delete-manga-trigger')

    if (edit_manga_trigger) {
        edit_manga_trigger.addEventListener('click', (e) => {
            e.preventDefault()
            edit_manga.classList.add('active')
            document.querySelector('.manga-info').classList.add('hidden')
        })
    }

    if (delete_manga_trigger) {
        delete_manga_trigger.addEventListener('click', (e) => {
            let body = new FormData()
            body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
            fetch(`/mangas/${delete_manga_trigger.dataset.id}`, {method: 'delete', body: body}).then(async resp => {
                resp = await resp.json()
                Turbolinks.visit('/mangas')
            })
        })
    }

    let add_original_link = edit_manga.querySelector('#add-original-link')
    add_original_link.addEventListener('click', (e) => {
        e.preventDefault()
        let new_input = document.createElement('div')
        new_input.className = 'link'
        new_input.innerHTML = `<input type="url" name="manga[original_links][]" placeholder="Ссылка на оригинал манги"> <a class="button remove-link"><i class="fa fa-trash"></i></a>`
        edit_manga.querySelector('#edit-original-links').insertBefore(new_input, add_original_link)
        new_input.querySelectorAll('.remove-link').forEach(trigger => {
            trigger.addEventListener('click', (e) => {
                e.preventDefault()
                trigger.parentElement.remove()
            })
        })
    })

    let add_translated_link = edit_manga.querySelector('#add-translated-link')
    add_translated_link.addEventListener('click', (e) => {
        e.preventDefault()
        let new_input = document.createElement('div')
        new_input.className = 'link'
        new_input.innerHTML = `<input type="url" name="manga[translated_links][]" placeholder="Ссылка на перевод манги"> <a class="button remove-link"><i class="fa fa-trash"></i></a>`
        edit_manga.querySelector('#edit-translated-links').insertBefore(new_input, add_translated_link)
        new_input.querySelectorAll('.remove-link').forEach(trigger => {
            trigger.addEventListener('click', (e) => {
                e.preventDefault()
                trigger.parentElement.remove()
            })
        })
    })

    document.querySelectorAll('.remove-link').forEach(trigger => {
        trigger.addEventListener('click', (e) => {
            e.preventDefault()
            console.log(trigger, trigger.parentElement)
            trigger.parentElement.remove()
        })
    })
}

export function initMangaCoverDND() {
    function preventDefaults(e) {
        e.preventDefault()
        e.stopPropagation()
    }

    let loadImage = document.querySelector('#load_cover')

    loadImage.addEventListener("change", (e) => {
        Rails.fire(loadImage.parentElement, 'submit')
    })

    let dropAreas = document.querySelectorAll(".dnd-image-loader, .manga-cover")
    dropAreas.forEach(dropArea => {
        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, preventDefaults, false)
        })

        ;['dragenter', 'dragover'].forEach(name => {
            dropArea.addEventListener(name, () => {
                dropArea.classList.add('active')
            })
        })
        ;['dragleave', 'drop'].forEach(name => {
            dropArea.addEventListener(name, () => {
                dropArea.classList.remove('active')
            })
        })
        dropArea.addEventListener('drop', (e) => {
            let loadImage = dropArea.parentElement.querySelector('.load_image_input')
            loadImage.files = e.dataTransfer.files
        })

        dropArea.addEventListener('click', e => dropArea.parentElement.querySelector('.load_image_input').click())
    })
}