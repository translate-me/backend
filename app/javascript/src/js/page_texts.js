export function deletePageText(id) {
    let body = new FormData()
    body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
    fetch(`/page_texts/${id}`, {method: 'delete', body: body}).then(async resp => {
        resp = await resp.json()
        reloadWithTurbolinks()
    })
}

export function setPageTextIndex(id, index) {
    let body = new FormData()
    body.append('authenticity_token', document.querySelector('meta[name="csrf-token"]').content)
    body.append('page_text[index]', index)
    fetch(`/page_texts/${id}`, {method: 'put', body: body}).then(async resp => {
        // reloadWithTurbolinks()
    })
}