/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

String.prototype.title = function() {return this.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())}
window.insertAfter = (elem, refElem) => {
    let parent = refElem.parentNode
    let next = refElem.nextSibling
    if (next) {
        return parent.insertBefore(elem, next)
    } else {
        return parent.appendChild(elem)
    }
}

let getColors = () => {
    return new Promise((resolve, reject) => {
        fetch('/users/colors').then(async (res) => {
            let data = await res.json()
            window.colors = data.result.colors
            resolve(colors)
        }).catch(reject)
    })
}

window.socket = io(SOCKETS_URL)

import "@babel/polyfill"

import Rails from 'rails-ujs';
import Turbolinks from 'turbolinks';

import initModals from '../src/js/modals'
window.initModals = initModals

import initChat from '../src/js/chat_messages'
window.initChat = initChat

import Editor from '../src/js/editor'
window.Editor = Editor

import dndIt from '../src/js/dndit'
window.dndIt = dndIt

require('../src/js/jscolor')

import { initChapterList, initMangaForm } from '../src/js/mangas'
window.initChapterList = initChapterList
window.initMangaForm = initMangaForm

import { initAddAndRemove, initTexts, initLeft } from "../src/js/pages"
window.initAddAndRemove = initAddAndRemove
window.initTexts = initTexts
window.initLeft = initLeft

import { deletePageText, setPageTextIndex } from '../src/js/page_texts'
window.deletePageText = deletePageText
window.setPageTextIndex = setPageTextIndex

window.Rails = Rails
Rails.start();
Turbolinks.start();

window.reloadWithTurbolinks = (function () {
    var winScrollPosition
    var elScrollPosition
    var selector

    function reload (sel) {
        if (sel) {
            selector = sel
            let el = document.querySelector(selector)
            elScrollPosition = [el.scrollLeft, el.scrollTop]
        }
        winScrollPosition = [window.scrollX, window.scrollY]
        Turbolinks.visit(window.location.toString(), { action: 'replace' })
    }

    document.addEventListener('turbolinks:load', function () {
        if (winScrollPosition) {
            window.scrollTo.apply(window, winScrollPosition)
            winScrollPosition = null
        }
        if (selector) {
            console.log('Scroll element', document.querySelector(selector), elScrollPosition)
            let el = document.querySelector(selector)
            el.scrollTo.apply(el, elScrollPosition)
            selector = null
        }
    })

    return reload
})()

document.addEventListener('turbolinks:load', function () {
    document.querySelectorAll('.jscolor').forEach(el=>window.jscolor(el))
})