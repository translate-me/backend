Rails.application.routes.draw do
  resources :mangas do
    resources :chapters, only: [:create, :show], constraints: { number: /\d+\.\d+/ }, param: :number do
      # get 'pages/:page_index', to: 'chapters#show'
    end
  end
  resources :chapters, only: [:destroy, :update]

  get '/logout', to: 'users#logout'

  post '/chapters/:chapter_id/pages', to: 'pages#create'

  post '/pages/:page_id/texts', to: 'page_texts#create'

  delete '/pages/:id', to: 'pages#destroy'

  get '/users/colors', to: 'users#colors'
  resources :users


  post '/users/login', to: 'users#login'

  post '/chapters/:chapter_id/messages', to: 'chat_messages#create'

  resources :pages, only: [:create, :update]

  resources :page_texts, only: [:update, :destroy]

  get '/', to: redirect('/mangas')

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
