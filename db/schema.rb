# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_03_184351) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "chapters", force: :cascade do |t|
    t.float "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "manga_id"
    t.string "title"
    t.index ["manga_id"], name: "index_chapters_on_manga_id"
  end

  create_table "chat_messages", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "chapter_id"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chapter_id"], name: "index_chat_messages_on_chapter_id"
    t.index ["user_id"], name: "index_chat_messages_on_user_id"
  end

  create_table "mangas", force: :cascade do |t|
    t.string "name"
    t.string "original_links", array: true
    t.string "translated_links", array: true
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "page_texts", force: :cascade do |t|
    t.string "original"
    t.string "translated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "page_id"
    t.integer "original_owners", array: true
    t.integer "translated_owners", array: true
    t.integer "index"
    t.boolean "is_translated"
    t.index ["page_id"], name: "index_page_texts_on_page_id"
  end

  create_table "pages", force: :cascade do |t|
    t.integer "index"
    t.bigint "chapter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chapter_id"], name: "index_pages_on_chapter_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teams_mangas", id: false, force: :cascade do |t|
    t.bigint "manga_id"
    t.bigint "team_id"
    t.index ["manga_id"], name: "index_teams_mangas_on_manga_id"
    t.index ["team_id"], name: "index_teams_mangas_on_team_id"
  end

  create_table "teams_users", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "team_id"
    t.index ["team_id"], name: "index_teams_users_on_team_id"
    t.index ["user_id"], name: "index_teams_users_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_salt"
    t.string "color"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "chapters", "mangas"
  add_foreign_key "chat_messages", "chapters"
  add_foreign_key "chat_messages", "users"
  add_foreign_key "page_texts", "pages"
  add_foreign_key "pages", "chapters"
end
