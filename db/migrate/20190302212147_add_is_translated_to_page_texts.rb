class AddIsTranslatedToPageTexts < ActiveRecord::Migration[5.2]
  def change
    add_column :page_texts, :is_translated, :boolean
  end
end
