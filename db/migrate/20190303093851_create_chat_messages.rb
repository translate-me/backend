class CreateChatMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_messages do |t|
      t.references :user, foreign_key: true
      t.references :chapter, foreign_key: true
      t.string :text
    end
  end
end
