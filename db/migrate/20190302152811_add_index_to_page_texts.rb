class AddIndexToPageTexts < ActiveRecord::Migration[5.2]
  def change
    add_column :page_texts, :index, :integer
  end
end
