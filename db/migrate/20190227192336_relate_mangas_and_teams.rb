class RelateMangasAndTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams_mangas, id: false do |t|
      t.belongs_to :manga, index: true
      t.belongs_to :team, index: true
    end
  end
end
