class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.integer :number
      t.references :chapter, foreign_key: true

      t.timestamps
    end
  end
end
