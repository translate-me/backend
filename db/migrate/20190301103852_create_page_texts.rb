class CreatePageTexts < ActiveRecord::Migration[5.1]
  def change
    create_table :page_texts do |t|
      t.string :original
      t.string :original_owners, array: true
      t.string :translated
      t.string :translated_owners, array: true

      t.timestamps
    end
  end
end
