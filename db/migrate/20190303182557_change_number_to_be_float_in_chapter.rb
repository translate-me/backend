class ChangeNumberToBeFloatInChapter < ActiveRecord::Migration[5.2]
  def change
    change_column :chapters, :number, :float
  end
end
