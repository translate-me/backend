class AddPageToPageText < ActiveRecord::Migration[5.1]
  def change
    add_reference :page_texts, :page, foreign_key: true
  end
end
