class CreateMangas < ActiveRecord::Migration[5.1]
  def change
    create_table :mangas do |t|
      t.string :name
      t.string :original_links, array: true
      t.string :translated_links, array: true
      t.string :description

      t.timestamps
    end
  end
end
