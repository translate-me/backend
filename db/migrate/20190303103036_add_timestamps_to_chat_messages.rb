class AddTimestampsToChatMessages < ActiveRecord::Migration[5.2]
  def change
    ChatMessage.all.each do |m|
      m.destroy
    end

    add_column :chat_messages, :created_at, :datetime, null: false
    add_column :chat_messages, :updated_at, :datetime, null: false
  end
end
