class RenamePageNumberToIndex < ActiveRecord::Migration[5.2]
  def change
    rename_column :pages, :number, :index
  end
end
