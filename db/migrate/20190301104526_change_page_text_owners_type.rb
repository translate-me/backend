class ChangePageTextOwnersType < ActiveRecord::Migration[5.1]
  def change
    remove_column :page_texts, :original_owners
    remove_column :page_texts, :translated_owners
    add_column :page_texts, :original_owners, :integer, array: true
    add_column :page_texts, :translated_owners, :integer, array: true
  end
end
