FROM ruby:latest
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs yarn > /dev/null
RUN nodejs -v
RUN mkdir /myapp
RUN bundler -v
RUN gem uninstall bundler
RUN gem install bundler
RUN bundler -v
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
ENV BUNDLE_PATH=/bundle \
    BUNDLE_BIN=/bundle/bin \
    GEM_HOME=/bundle
ENV PATH="${BUNDLE_BIN}:${PATH}"
#RUN bundle update --bundler
RUN bundle install
COPY . /myapp

CMD ["bundle", "exec", "rails", "s", "-p", "3030", "-b", "0.0.0.0"]
