require 'test_helper'

class PageTextsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get page_texts_create_url
    assert_response :success
  end

  test "should get destroy" do
    get page_texts_destroy_url
    assert_response :success
  end

end
